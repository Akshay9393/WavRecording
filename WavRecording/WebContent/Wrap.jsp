<%@ page language="java" contentType="application/VXML+xml; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<?xml version="1.0" encoding="UTF-8"?>
<vxml version="2.0" xmlns="http://www.w3.org/2001/vxml" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.w3.org/2001/vxml 
   http://www.w3.org/TR/voicexml20/vxml.xsd">
   <property name="audiomaxage" value="1s"/>
   <property name="audiomaxstale" value="1s"/>
   
   
   <form id="thankyou">
   <block>
   <prompt bargein="false">
   <audio expr="'prompts/conclusion.wav'"></audio>
   </prompt>
   <disconnect/>
    </block>
   </form>
   </vxml>