<%@ page language="java" contentType="application/VXML+xml; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<?xml version="1.0" encoding="UTF-8"?>
<vxml version="2.0" xmlns="http://www.w3.org/2001/vxml" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.w3.org/2001/vxml 
   http://www.w3.org/TR/voicexml20/vxml.xsd">
   
   <script>
    <![CDATA[
     var num;
   ]]>
   
   </script>
   
   <property name="grammarmaxage" value="1s"/>
   <property name="grammarmaxstale" value="1s"/>
   <property name="audiomaxage" value="1s"/>
   <property name="audiomaxstale" value="1s"/>
   
   <form id = "record">
   <block>
   		<prompt>
   			<audio expr="'prompts/prompt1.wav'"/>
   		</prompt>
   </block>
   <field name = "PhoneNum">
    <grammar type="application/srgs+xml" srcexpr="'grammar/en-in/fc41500_CollectBookingReferenceNumber_DM_dtmf.grxml'"></grammar>
    <grammar type="application/srgs+xml" srcexpr="'grammar/en-in/fc41500_CollectBookingReferenceNumber_DM.grxml'"></grammar>
   <prompt>
     <audio expr="'prompts/prompt2.wav'"/>
    </prompt>
     <filled>
    <assign name="num" expr="phno.SWI_meaning"/>
  		<goto next="#recor"/>
  	</filled>
    
   </field> 
 </form>
 
 <form id = "recor">
 	<record name = "uservoice" beep = "true" type="audio/x-wav">
 		<prompt>
   				<audio expr="'prompts/prompt3.wav'"/>
   		</prompt>
 	<filled>
   		<prompt>
    			<audio expr="uservoice"/>
  		</prompt>
  			<submit next="Audio_Process" namelist="num uservoice" method="post" enctype="multipart/form-data"/>
  	</filled>
  </record>
 </form>
</vxml> 