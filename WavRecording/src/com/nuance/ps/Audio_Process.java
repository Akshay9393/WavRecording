package com.nuance.ps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


@WebServlet("/Audio_Process")
public class Audio_Process extends HttpServlet implements Serializable {
	
	private static final long serialVersionUID = 1L;
       
	Object a;
	
	public void demo(Object c) {
		
		this.a = c;
	}
  
    public Audio_Process() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final Part filePart = request.getPart("voice");
		System.out.println(request.getParameter("num"));
		System.out.println(filePart.getContentType());
		LocalDate currentDate = LocalDate.now();
		int day = currentDate.getDayOfMonth();
		Month month = currentDate.getMonth();
		int year = currentDate.getYear();
		System.out.println(month);
		String path = "C:/Users/akshay.bhaskar/Videos/Recordings"+String.valueOf(year);
		String path1 = path+"/"+month.toString();
		String path2 = path1+"/"+String.valueOf(day);
		System.out.println(path);
		
		File file = new File(path);
		
		//LocalDateTime dateTime = LocalDateTime.now();
		
		if(!file.exists()) {
			file.mkdir();
		}
		File file1 = new File(path1);
		if (!file1.exists()) {
			
			file1.mkdir();
		}
		File file2 = new File(path2);
		if(!file.exists()) {
			
			file2.mkdir();
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		Date date = new Date();
		String path3 = path2+"/"+request.getParameter("num")+"-"+ dateFormat.format(date).toString()+".wav";
		OutputStream output = null;
		InputStream input = null;
		
		output = new FileOutputStream(new File(path3));
		input = filePart.getInputStream();
		int read = 0;
		final byte[] bytes = new byte[32*1024];
		
		while((read=input.read(bytes))!=-1) {
			
			output.write(bytes, 0, read);
		}
		
		output.flush();
		output.close();
		RequestDispatcher dispatcher = request.getRequestDispatcher("Wrap.jsp");
		dispatcher.forward(request, response);
	}

}
